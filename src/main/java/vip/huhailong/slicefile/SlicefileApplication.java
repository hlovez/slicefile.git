package vip.huhailong.slicefile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SlicefileApplication {

	public static void main(String[] args) {
		SpringApplication.run(SlicefileApplication.class, args);
	}

}
